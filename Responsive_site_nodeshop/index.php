<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	
 	<link rel="stylesheet" href="css/style.css">
 	<script src="js/scripts.js"></script>
 	<script src="https://use.fontawesome.com/daee7816e8.js"></script>
 	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
 	<title>
 		NodeShop - Loja de informática
 	</title>

</head>

<body>

<!-- Header principal -->
<header class="cabecalho">
	<h1 class="logo">
		<a href="index.html" title="NodeShop - Loja de Informática">
			NodeShop - Loja de Informática
		</a>
	</h1>

	<form method="post">
		<input type="text" name="search" id="search" placeholder="Faça uma busca">
		<button>
			<i class="fa fa-search fa-lg" aria-hidden="true"></i>
		</button>
	</form>
</header>

<nav class="menu">
	<ul>
		<li><a href="#">Home</a></li>
		<li><a href="#">Produtos</a></li>
		<li><a href="#">Serviços</a></li>
		<li><a href="#">Contato</a></li>
	</ul>
	<div class="social-icons">
		<a href="#" class="btn-facebook"><i class="fa fa-facebook fa-lg"></i></a>
		<a href="#" class="btn-twitter"><i class="fa fa-twitter fa-lg"></i></a>
		<a href="#" class="btn-google"><i class="fa fa-google fa-lg"></i></a>
	</div>
</nav>

<main class="principal">
	<article class="sobre">
		<h2>Sobre nós</h2>
		<img src="img\loja.jpg" alt="NodeShop">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque veritatis suscipit accusantium, earum possimus perferendis id! Accusantium consequatur eaque vel. Repellendus ipsam eveniet doloribus suscipit, numquam neque fuga, incidunt! Labore.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit optio fugit cupiditate velit quibusdam eos perspiciatis, nemo consectetur aperiam molestias tempore eligendi natus sequi. Illo consectetur aut perspiciatis maxime ea!
		</p>
	</article>

	<aside class="onde-estamos">
		<h2>Onde estamos</h2>
		<p>Av. Dr. Júlio Rodrigues, 122 - Teófilo Otoni / MG</p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15189.173555291782!2d-41.5082145!3d-17.8717298!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4c2ce9232e79b43f!2sCOMSEG+-+ENGENHARIA+DE+SEGURAN%C3%87A+E+MEDICINA+DO+TRABALHO!5e0!3m2!1spt-BR!2sbr!4v1486115381262"></iframe>
		<h2>Contatos</h2>
		<ul>
			<li><i class="fa fa-phone fa-lg"></i>(33) 99109-9067</li>
			<li><i class="fa fa-whatsapp fa-lg"></i>(73) 99109-9067</li>
			<li><i class="fa fa-envelope fa-lg"></i>marlontf@hotmail.com</li>
		</ul>
	</aside>
</main>

<section class="newsletter">
	<h3>Newsletter</h3>
	<p>Receba nossas promoções por e-mail</p>
	<form method="post">
		<input type="text" placeholder="Seu nome">
		<input type="email" placeholder="Seu e-mail">
		<button>Cadastrar</button>
	</form>
</section>

<footer class="rodape">
	<p>© NodeShop - Todos os direitos reservados</p>
</footer>

</body>
 
</html>