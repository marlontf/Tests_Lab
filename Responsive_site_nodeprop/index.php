<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>NodeProp - Especializado em Soluções Digitais</title>
	<meta name="description" content="Agência especializada em Marketing Digital, Criação de sites e Aplicativos Mobile">
	<meta name="keyword" content="Agência digital, Marketing, Sites">
	<meta name="robots" content="index, follow">
	<meta name="author" content="Marlon frança">
	<link rel="stylesheet" href="css/style.css">
	<script src="https://use.fontawesome.com/daee7816e8.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 	<link href="https://fonts.googleapis.com/css?family=Lato:400,300,700" rel="stylesheet" type="text/css">
 	<link rel="icon" href="img/icon.png">
</head>
<body>
	
<header class="cabecalho">
	<a href="index.php"><h1 class="logo">NodeProp - Especializado em Soluções Digitais</h1></a>
	<button class="btn-menu">
		<li class="fa fa-bars fa-lg"></li>
	</button>
	<nav class="menu">
		<a class="btn-close"><i class="fa fa-times"></i></a>
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Clientes</a></li>
			<li><a href="#">Serviços</a></li>
			<li><a href="#">Blog</a></li>
			<li><a href="#">Quem somos</a></li>
			<li><a href="#">Contato</a></li>
		</ul>
	</nav>
</header>
<script>
		$('.btn-menu').click(function(){
			$('.menu').show();
		});
		$('.btn-close').click(function(){
			$('.menu').hide();
		});
</script>
<div class="banner">
	<div class="title">
		<h2>Ouse inovar!</h2>
		<h3>Criamos experiências e estabelecemos ações estratégicas que conectam marcas e consumidores</h3>
	</div>
	<div class="buttons">
		<button class="btn-cadastrar">Cadastrar <i class="fa fa-arrow-circle-right"></i></button>
		<button class="btn-sobre">Sobre <i class="fa fa-question-circle"></i></button>
	</div>
</div>
<main class="servicos">
	<article class="servico">
		<a href="#"><img src="img/criacoes.jpg" alt="Campanhas publicitárias"></a>
		<div class="inner">
			<a href="#">Campanhas publicitárias</a>
			<h4>Impressos, VTs e Jingles</h4>
			<p>Se você está precisando de criação de algum material específico, conte com a nossa equipe de profissionais. Eles farão toda campanha publicitária. VT, outdoor, folder, anúncio e muito mais pela sua empresa no mais alto padrão de qualidade.</p>
		</div>
	</article>
	<article class="servico">
		<a href="#"><img src="img/md.jpg" alt="Marketing digital"></a>
		<div class="inner">
			<a href="#">Marketing digital</a>
			<h4>Administração de redes sociais</h4>
			<p>Como agência de publicidade aplicamos estratégias nos meios digitais para que o seu negócio seja visto por milhões de usuários. O Brasil é o 5º país mais conectado do mundo. Por este motivo, o seu negócio não pode ficar fora do mercado digital.</p>
		</div>
	</article>
	<article class="servico">
		<a href="#"><img src="img/cs.jpg" alt="Criação de sites"></a>
		<div class="inner">
			<a href="#">Criação de sites</a>
			<h4>Sites administráveis</h4>
			<p>Agora você pode administrar o seu site quando quiser. E melhor ainda pois você pode pagar por este serviço, pois desenvolvemos de forma. Seu site atualizado, com seus últimos produtos, integração com redes sociais, agora é possível.</p>
		</div>
	</article>
</main>

<section class="newsletter">
	<div>
		<h2>Inscreva-se Agora</h2>
		<h3>Receba novidades, dicas e muito mais</h3>
	</div>
	<div>
		<form>
			<input type="email" name="email" placeholder="Seu email">
			<button>Cadastrar</button>
		</form>
	</div>
</section>

<footer class="rodape">
	<div class="social-icons">
		<a href="#"><i class="fa fa-facebook"></i></a>
		<a href="#"><i class="fa fa-twitter"></i></a>
		<a href="#"><i class="fa fa-google"></i></a>
		<a href="#"><i class="fa fa-instagram"></i></a>
		<a href="#"><i class="fa fa-envelope"></i></a>
	</div>
	<p class="copyright">
		Copyright © NodeProp 2017 - Todos os direitos reservados
	</p>
</footer>

</body>
</html>